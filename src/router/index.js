import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import TodayTodos from '../views/TodayTodos.vue'
import Tommorow from '../views/Tommorow.vue'
import Later from '../views/Later.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Today',
    name: 'Today',
    component: TodayTodos
  },
  {
    path: '/Tommorow',
    name: 'Today',
    component: Tommorow
  },
  {
    path: '/Later',
    name: 'Later',
    component: Later
  },
]

const router = new VueRouter({
  routes
})

export default router
